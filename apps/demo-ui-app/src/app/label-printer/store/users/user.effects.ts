import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import { Observable, of } from 'rxjs';

import * as fromActions from './user.actions';
import { UserFetchersService } from '../../services/user-fetchers.service';
/**
 * service is use handle dispatched action for user asynchronously ie(user)
 */
@Injectable()
export class UserEffects {
  constructor(
    private actions$: Actions,
    private userFetchersService : UserFetchersService
  ) {}
  @Effect()
  loadUsers$: Observable<Action> = this.actions$.ofType(fromActions.UserActionTypes.LoadUsers).pipe(
    switchMap((payload): any => {
      /*return this.userFetchersService.getUsers().pipe(
        map(res => {
          return new fromActions.LoadUsersSuccess({ users: res.data });
        }),
        catchError(error => {
          return of({ type: 'FAIL' });
        })
      );*/
    })
  );
 /* @Effect()
  addUser$: Observable<Action> = this.actions$.ofType<fromActions.AddUser>(fromActions.UserActionTypes.AddUser).pipe(
    map(action => action.payload),
    mergeMap(payload =>
      this.accountService.addUser(payload).pipe(
        map(res => {
          if (Number(res.status) === AccountConstants.NOT_ABLE_TO_ADD_USER) {
            return { type: 'NOT ABLE TO ADD USER' };
          } else {
            return new fromActions.AddUserSuccess({ user: res.data });
          }
        }),
        catchError(error => of({ type: 'FAIL' }))
      )
    )
  );
  @Effect()
  loadUsers$: Observable<Action> = this.actions$.ofType(fromActions.UserActionTypes.LoadUsers).pipe(
    switchMap((payload): any => {
      return this.accountService.loadUsers(payload).pipe(
        map(res => {
          return new fromActions.LoadUsersSuccess({ users: res.data });
        }),
        catchError(error => {
          return of({ type: 'FAIL' });
        })
      );
    })
  );
  @Effect()
  loadUsersForProject$: Observable<Action> = this.actions$.ofType(fromActions.UserActionTypes.LoadUsersForProject).pipe(
    switchMap((payload): any => {
      return this.projectService.loadUsers(payload).pipe(
        map(res => {
          return new fromActions.LoadUsersSuccess({ users: res.data });
        }),
        catchError(error => {
          return of({ type: 'FAIL' });
        })
      );
    })
  );
  @Effect()
  deleteUserAccount$: Observable<Action> = this.actions$.ofType(fromActions.UserActionTypes.DeleteUserAccount).pipe(
    switchMap((payload): any => {
      return this.accountService.deleteUserAccount(payload).pipe(
        map(res => {
          return new fromActions.LoadUsers({ id: this.accountService.selectedAccountId });
        }),
        catchError(error => {
          return of({ type: 'FAIL' });
        })
      );
    })
  );
  @Effect()
  deleteUserProject$: Observable<Action> = this.actions$.ofType(fromActions.UserActionTypes.DeleteUserProject).pipe(
    switchMap((payload): any => {
      return this.projectService.deleteUserProject(payload).pipe(
        map(res => {
          return new fromActions.LoadUsersForProject({ id: this.projectService.selectedProjectId });
        }),
        catchError(error => {
          return of({ type: 'FAIL' });
        })
      );
    })
  );
  @Effect()
  updateUserAccountRole$: Observable<Action> = this.actions$
    .ofType(fromActions.UserActionTypes.UpdateUserAccountRole)
    .pipe(
      switchMap((payload): any => {
        return this.accountService.updateUserAccountRole(payload).pipe(
          map(res => {
            this.projectStore.loadProjects(this.accountService.selectedAccountId);

            return new fromActions.UpdateUserSuccess();
          }),
          catchError((error: any): any => {
            if (this.accountService.selectedAccountId) {
              this.userStore.dispatch(new fromActions.LoadUsers({ id: this.accountService.selectedAccountId }));
            }

            return of({ type: 'FAIL' });
          })
        );
      })
    );
  @Effect()
  updateUserProjectRole$: Observable<Action> = this.actions$
    .ofType(fromActions.UserActionTypes.UpdateUserProjectRole)
    .pipe(
      switchMap((payload): any => {
        return this.projectService.updateUserProjectRole(payload).pipe(
          map(res => {
            // tslint:disable-next-line:no-commented-code
            // this.projectStore.loadUsers(this.projectService.selectedProjectId);

            return new fromActions.UpdateUserSuccess();
          }),
          catchError((error: any): any => {
            if (this.projectService.selectedProjectId) {
              this.userStore.dispatch(
                new fromActions.LoadUsersForProject({ id: this.projectService.selectedProjectId })
              );
            }

            return of({ type: 'FAIL' });
          })
        );
      })
    );*/
}
