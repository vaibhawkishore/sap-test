import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { User } from './user.model';
import { UserActions, UserActionTypes } from './user.actions';
import { createFeatureSelector, createSelector } from '@ngrx/store';
export interface State extends EntityState<User> {
  // additional entities state properties
}
export function getUserId(user: User): number {
  return user.id;
}
export const adapter: EntityAdapter<User> = createEntityAdapter<User>({
  selectId: getUserId,
  sortComparer: false
});
export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});
export function reducer(state = initialState, action: UserActions): State {
  switch (action.type) {
    case UserActionTypes.AddUser:{
      return adapter.addOne(action.payload.user, state);
    }
   /* case UserActionTypes.AddUserSuccess: {
      return adapter.addOne(action.payload.user, state);
    }*/

    case UserActionTypes.AddUsers: {
      return adapter.addMany(action.payload.users, state);
    }

    case UserActionTypes.UpdateUser: {
      return adapter.updateOne(action.payload.user, state);
    }

    case UserActionTypes.UpdateUsers: {
      return adapter.updateMany(action.payload.users, state);
    }

    case UserActionTypes.DeleteUser: {
      return adapter.removeOne(action.payload.id, state);
    }

    case UserActionTypes.DeleteUsers: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case UserActionTypes.LoadUsersSuccess: {
      return adapter.addAll(action.payload.users, state);
    }

    case UserActionTypes.ClearUsers: {
      return adapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}
export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();
export const getUserState = createFeatureSelector<State>('user');
export const selUserIds = createSelector(getUserState, selectIds);
export const selUserEntities = createSelector(getUserState, selectEntities);
export const selAllUsers = createSelector(getUserState, selectAll);
export const usersCount = createSelector(getUserState, selectTotal);
