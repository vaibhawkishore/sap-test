import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { User } from './user.model';

export enum UserActionTypes {
  LoadUsers = '[User] Load Users',
  LoadUsersForProject = '[User] Load Users For Project',
  UpdateUserAccountRole = '[User] Update User Account Role',
  UpdateUserProjectRole = '[User] Update User Project Role',
  LoadUsersSuccess = '[User] Load Users Success',
  AddUser = '[User] Add User',
  AddUserSuccess = '[User] Add User Success',
  AddUserFailure = '[User] Add User Failure',
  AddUsers = '[User] Add Users',
  UpdateUser = '[User] Update User',
  UpdateUsers = '[User] Update Users',
  DeleteUser = '[User] Delete User',
  DeleteUserAccount = '[User] Delete User Account',
  DeleteUserProject = '[User] Delete User Project',
  DeleteUsers = '[User] Delete Users',
  ClearUsers = '[User] Clear Users',
  UpdateUserSuccess = '[User] Update User Success '
}

export class LoadUsers implements Action {
  readonly type = UserActionTypes.LoadUsers;

  constructor() {}
}
export class LoadUsersForProject implements Action {
  readonly type = UserActionTypes.LoadUsersForProject;

  constructor(public payload: { id: number }) {}
}
export class UpdateUserSuccess implements Action {
  readonly type = UserActionTypes.UpdateUserSuccess;

  constructor() {}
}

export class DeleteUserAccount implements Action {
  readonly type = UserActionTypes.DeleteUserAccount;

  constructor(public payload: { id: number }) {}
}
export class DeleteUserProject implements Action {
  readonly type = UserActionTypes.DeleteUserProject;

  constructor(public payload: { projectId: number; userId: number }) {}
}
export class LoadUsersSuccess implements Action {
  readonly type = UserActionTypes.LoadUsersSuccess;

  constructor(public payload: { users: Array<User> }) {}
}
export class UpdateUserAccountRole implements Action {
  readonly type = UserActionTypes.UpdateUserAccountRole;

  constructor(public payload: { data: any }) {}
}
export class UpdateUserProjectRole implements Action {
  readonly type = UserActionTypes.UpdateUserProjectRole;

  constructor(public payload: { data: any }) {}
}

export class AddUser implements Action {
  readonly type = UserActionTypes.AddUser;

  constructor(public payload: { user: User; addFrom?: string }) {}
}
export class AddUsers implements Action {
  readonly type = UserActionTypes.AddUsers;

  constructor(public payload: { users: Array<User> }) {}
}
export class AddUserSuccess implements Action {
  readonly type = UserActionTypes.AddUserSuccess;

  constructor(public payload: { user: User }) {}
}
export class AddUserFailure implements Action {
  readonly type = UserActionTypes.AddUserFailure;
  constructor(public payload: any) {}
}

export class UpdateUser implements Action {
  readonly type = UserActionTypes.UpdateUser;

  constructor(public payload: { user: Update<User> }) {}
}

export class UpdateUsers implements Action {
  readonly type = UserActionTypes.UpdateUsers;

  constructor(public payload: { users: Array<Update<User>> }) {}
}

export class DeleteUser implements Action {
  readonly type = UserActionTypes.DeleteUser;

  constructor(public payload: { id: string }) {}
}

export class DeleteUsers implements Action {
  readonly type = UserActionTypes.DeleteUsers;

  constructor(public payload: { ids: Array<string> }) {}
}

export class ClearUsers implements Action {
  readonly type = UserActionTypes.ClearUsers;
}

export type UserActions =
  | UpdateUserSuccess
  | LoadUsers
  | LoadUsersSuccess
  | AddUser
  | AddUserSuccess
  | AddUserFailure
  | AddUsers
  | UpdateUser
  | UpdateUsers
  | DeleteUser
  | DeleteUsers
  | ClearUsers
  | DeleteUserProject
  | LoadUsersForProject;
