import { TestBed } from '@angular/core/testing';

import { UserFetchersService } from './user-fetchers.service';

describe('UserFetchersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserFetchersService = TestBed.get(UserFetchersService);
    expect(service).toBeTruthy();
  });
});
