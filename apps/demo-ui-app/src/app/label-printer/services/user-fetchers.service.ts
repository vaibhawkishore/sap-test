import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../store/users/user.model';
import { Observable, Subject } from 'rxjs';
@Injectable()
export class UserFetchersService {
  private amount = new Subject<number>();
  amount$ = this.amount.asObservable();
  constructor(private http:HttpClient) { }
  public setMoney(value:number) {
    this.amount.next(value);
  }
  getNotes(money:number):any{
    const notesArray:Array<number>=[2000,500,200,100,50,20,10,5,2,1];
    let notesMap=new Map(
    );
    for(let value of notesArray){
      const note:number=money/value;
      if(money>0){
        money=money%value;
      }

      notesMap.set(value,Math.floor(note));
    }
    
    return notesMap;
  }
  
}

