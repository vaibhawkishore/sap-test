import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LableSteperFormComponent } from '../lable-steper-form/lable-steper-form.component';

@Component({
  selector: 'ui-suite-basic-dialog',
  templateUrl: './basic-dialog.component.html',
  styleUrls: ['./basic-dialog.component.scss']
})
export class BasicDialogComponent implements OnInit {
  ngOnInit() {}
}
