import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BasicDialogComponent } from '../basic-dialog/basic-dialog.component';
import { MatHorizontalStepper } from '@angular/material/stepper';
import { Store } from '@ngrx/store';
import  * as fromUserReducer from '../../store/users/user.reducer';
import { AddUser } from '../../store/users/user.actions';

@Component({
  selector: 'ui-suite-lable-steper-form',
  templateUrl: './lable-steper-form.component.html',
  styleUrls: ['./lable-steper-form.component.scss']
})
export class LableSteperFormComponent implements OnInit {
  @ViewChild('stepper')
  public stepper: MatHorizontalStepper;
  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  
  constructor(
    private userStore: Store<fromUserReducer.State>,
    private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<BasicDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }
  closeDialog() {
    this.dialogRef.close();
    const id = new Date().getUTCMilliseconds();
    this.userStore.dispatch(new AddUser({user:{id:id,name:this.firstFormGroup.value.firstCtrl,address:this.secondFormGroup.value.secondCtrl}}));
    this.stepper.reset();
  }
}
