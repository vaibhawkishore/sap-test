import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LableSteperFormComponent } from './lable-steper-form.component';

describe('LableSteperFormComponent', () => {
  let component: LableSteperFormComponent;
  let fixture: ComponentFixture<LableSteperFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LableSteperFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LableSteperFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
