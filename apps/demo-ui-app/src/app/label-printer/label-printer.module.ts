import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewLableGeneratorComponent } from './pages/new-lable-generator/new-lable-generator.component';
import { SharedModule } from '@ui-suite/shared';
import { BasicDialogComponent } from './components/basic-dialog/basic-dialog.component';
import * as fromUser from './store/users/user.reducer';
import { LableSteperFormComponent } from './components/lable-steper-form/lable-steper-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UserEffects } from './store/users/user.effects';
import { UserFetchersService } from './services/user-fetchers.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    SharedModule,
    StoreModule.forFeature('user', fromUser.reducer),
    EffectsModule.forFeature([UserEffects])
  ],
  entryComponents: [BasicDialogComponent, LableSteperFormComponent],
  declarations: [
    NewLableGeneratorComponent,
    BasicDialogComponent,
    LableSteperFormComponent
  ],
  exports: [NewLableGeneratorComponent],
  providers: [UserFetchersService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class LabelPrinterModule {}
