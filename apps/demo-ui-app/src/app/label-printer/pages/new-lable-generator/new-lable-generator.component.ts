import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BasicDialogComponent } from '../../components/basic-dialog/basic-dialog.component';
import { User } from '../../store/users/user.model';
import  * as fromUserReducer from '../../store/users/user.reducer';
import { Observable } from 'rxjs/internal/Observable';
import { Store } from '@ngrx/store';
import { AddUser, LoadUsers } from '../../store/users/user.actions';
import { UserFetchersService } from '../../services/user-fetchers.service';

@Component({
  selector: 'ui-suite-new-lable-generator',
  templateUrl: './new-lable-generator.component.html',
  styleUrls: ['./new-lable-generator.component.scss']
})
export class NewLableGeneratorComponent implements OnInit {
  notes: Array<any>=[];
  displayedColumns: string[] = ['first'];
  constructor(private userFeatcher: UserFetchersService) {
   this.notes=[
  ];
   }
  

  ngOnInit() {
    this.userFeatcher.amount$.subscribe(amt=>{
      const calculatedNotes= this.userFeatcher.getNotes(amt);
      const notesArray:Array<number>=[2000,500,200,100,50,20,10,5,2,1];
      const notesFormat=[];
      let total=0;
      for(var key of notesArray){
        const value=calculatedNotes.get(key);
          notesFormat.push({first:`${value} notes of Rs ${key}`});
          total=total+value;
      }
      notesFormat.push({first:`Total notes dispensed : ${total}`});
         
       this.notes=notesFormat;
    });
  }
}
