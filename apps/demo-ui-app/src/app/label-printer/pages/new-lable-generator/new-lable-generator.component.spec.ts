import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewLableGeneratorComponent } from './new-lable-generator.component';

describe('NewLableGeneratorComponent', () => {
  let component: NewLableGeneratorComponent;
  let fixture: ComponentFixture<NewLableGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewLableGeneratorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewLableGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
