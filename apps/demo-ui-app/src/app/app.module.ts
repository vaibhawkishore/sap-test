import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MatButtonModule, MatCheckboxModule } from '@angular/material';

import { AppComponent } from './app.component';
import { NxModule } from '@nrwl/nx';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LabelPrinterModule } from './label-printer/label-printer.module';
import { NewLableGeneratorComponent } from './label-printer';
import { SharedModule } from '@ui-suite/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';
import { metaReducers,reducers } from './app.reducer';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UserFetchersService } from './label-printer/services/user-fetchers.service';
const routes: Routes = [
  { path: 'addLabel', component: NewLableGeneratorComponent },
  { path: '', redirectTo: '/addLabel', pathMatch: 'full' }
];
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    MatButtonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    LabelPrinterModule,
    SharedModule,
    MatCheckboxModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot([]),
    NxModule.forRoot(),
    RouterModule.forRoot(routes, { initialNavigation: 'enabled' }),
    BrowserAnimationsModule,
  ],
  providers: [UserFetchersService],
  bootstrap: [AppComponent]
})
export class AppModule {}
