import { ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';
import { EntityState } from '@ngrx/entity';

import { environment } from '../environments/environment';

// tslint:disable-next-line:no-empty-interface
export interface AppState {
}

export const reducers: ActionReducerMap<AppState> = {
  
};
export function logger(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
  return (state: AppState, action: any): AppState => {
    return reducer(state, action);
  };
}
export const metaReducers: Array<MetaReducer<AppState>> = !environment.production ? [logger] : [];
