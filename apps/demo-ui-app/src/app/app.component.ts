import { Component } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { UserFetchersService } from './label-printer/services/user-fetchers.service';
@Component({
  selector: 'ui-suite-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ATM Money Dispenser';

  atmForm = new FormGroup({
    amount: new FormControl('', Validators.required)
  });
  constructor(private userFeatcher: UserFetchersService){}
  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.userFeatcher.setMoney(this.atmForm.value.amount);
  }
}
